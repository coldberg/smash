#include "rle.hpp"
#include <cassert>

std::size_t Rle::count_run_length (const std::vector<std::uint8_t>& s_data, std::size_t s_offset) {
    const auto s_begin = s_offset;
    while (s_offset < s_data.size () - 1u
        && s_offset - s_begin < st_max_run_length - 1u
        && s_data [s_offset] == s_data [s_offset + 1u]) {
        ++s_offset;
    }
    return s_offset < s_data.size () ? s_offset - s_begin + 1u : 0u;
}

void Rle::encode (std::vector<std::uint8_t>& s_data) {
    std::vector<std::uint8_t> s_rle_buff;
    s_rle_buff.reserve (s_data.size ());
    std::size_t s_offset = 0u;
    std::size_t s_begin = 0u;

    while (s_offset < s_data.size ()) {
        auto s_run = count_run_length (s_data, s_offset);
        if (s_run > 0u && s_run < st_thresshold) {
            s_offset += s_run;
            if (s_offset < s_data.size ()) {
                continue;
            }
        }

        while (s_offset - s_begin > 0u) {
            auto s_len = std::min<std::size_t> (128u, s_offset - s_begin);
            s_rle_buff.push_back ((std::uint8_t) ((s_len - 1u) | 0x80));
            auto s_it = s_data.begin () + s_begin;
            s_rle_buff.insert (s_rle_buff.end (), s_it, s_it + s_len);
            s_begin += s_len;
        }

        while (s_run >= st_thresshold) {
            s_rle_buff.push_back ((std::uint8_t) (s_run - 1u));
            s_rle_buff.push_back (s_data [s_offset]);
            s_offset += s_run;
            s_run = count_run_length (s_data, s_offset);
        }
        s_begin = s_offset;
    }
    s_data = std::move (s_rle_buff);
}

void Rle::decode (std::vector<std::uint8_t>& s_data) {
    std::vector<std::uint8_t> s_raw_buff;
    s_raw_buff.reserve (s_data.size () * 2u);
    std::size_t s_offset = 0u;
    while (s_offset < s_data.size ()) {
        if (s_data [s_offset] & 0x80) {
            const auto s_len = (s_data [s_offset] & 0x7f) + 1u;
            s_offset += 1u;
            auto s_it = s_data.begin () + s_offset;
            s_raw_buff.insert (s_raw_buff.end (), s_it, s_it + s_len);
            s_offset += s_len;
            continue;
        }

        for (auto i = 0u; i <= s_data [s_offset]; ++i) {
            s_raw_buff.push_back (s_data [s_offset + 1u]);
        }
        s_offset += 2u;
    }
    s_data = std::move (s_raw_buff);
}

void Rle::Tests::count_run_length (const std::vector<std::uint8_t>& s_test0) {

    std::size_t s_result = 0u;
    assert ((s_result = Rle::count_run_length (s_test0, 0)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 1)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 2)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 3)) == 4);
    assert ((s_result = Rle::count_run_length (s_test0, 4)) == 3);
    assert ((s_result = Rle::count_run_length (s_test0, 5)) == 2);
    assert ((s_result = Rle::count_run_length (s_test0, 6)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 7)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 8)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 9)) == 3);
    assert ((s_result = Rle::count_run_length (s_test0, 10)) == 2);
    assert ((s_result = Rle::count_run_length (s_test0, 11)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 12)) == 4);
    assert ((s_result = Rle::count_run_length (s_test0, 13)) == 3);
    assert ((s_result = Rle::count_run_length (s_test0, 14)) == 2);
    assert ((s_result = Rle::count_run_length (s_test0, 15)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 16)) == 2);
    assert ((s_result = Rle::count_run_length (s_test0, 17)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 18)) == 2);
    assert ((s_result = Rle::count_run_length (s_test0, 19)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 20)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 21)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 22)) == 2);
    assert ((s_result = Rle::count_run_length (s_test0, 23)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 24)) == 128);
    assert ((s_result = Rle::count_run_length (s_test0, 50)) == 128);
    assert ((s_result = Rle::count_run_length (s_test0, 110)) == 100);
    assert ((s_result = Rle::count_run_length (s_test0, 209)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 210)) == 1);
    assert ((s_result = Rle::count_run_length (s_test0, 214)) == 0);
}

inline void Rle::Tests::encode (std::vector<std::uint8_t>& s_data, const std::vector<std::uint8_t>& s_targ) {
    Rle::encode (s_data);
    assert (s_data.size () == s_targ.size ());
    const auto s_len = std::min (s_data.size (), s_targ.size ());
    for (auto i = 0u; i < s_len; ++i) {
        assert (s_data [i] == s_targ [i]);
    }
}

void Rle::Tests::decode (std::vector<std::uint8_t>& s_data, const std::vector<std::uint8_t>& s_orig) {
    Rle::decode (s_data);
    assert (s_data.size () == s_orig.size ());
    const auto s_len = std::min (s_data.size (), s_orig.size ());
    for (auto i = 0u; i < s_len; ++i) {
        assert (s_data [i] == s_orig [i]);
    }
}

void Rle::Tests::run () {
    std::vector<std::uint8_t> s_test0 = {
        //   0, 1, 2, 3, 4, 5, 6, 7, 8, 9,     
        1, 2, 3, 4, 4, 4, 4, 1, 2, 3,  // 0*
        3, 3, 1, 1, 1, 1, 2, 2, 3, 3,  // 1*
        1, 2, 3, 3, 1, 1, 1, 1, 1, 1,  // 2*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 3*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 4*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 5*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 6*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 7*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 8*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 9*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 10*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 11*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 12*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 13*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 14*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 15*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 16*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 17*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 18*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 19*
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // 20*
        4, 3, 2, 1
    };

    std::vector<std::uint8_t> s_comp0 = {
        0x82, 1, 2, 3,
        0x03, 4,
        0x81, 1, 2,
        0x02, 3,
        0x03, 1,
        0x87, 2, 2, 3, 3, 1, 2, 3, 3,
        0x7F, 1,
        0x39, 1,
        0x83, 4, 3, 2, 1
    };

    count_run_length (s_test0);
    auto s_test1 = s_test0;
    encode (s_test1, s_comp0);
    decode (s_test1, s_test0);
}
