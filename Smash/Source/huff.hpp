#pragma once

#include <vector>
#include <cstddef>
#include <cstdint>
#include <tuple>

struct Huff {   
    static void encode (std::vector<std::uint8_t>& s_data);
    static void decode (std::vector<std::uint8_t>& s_data);
};