#pragma once

#include <vector>
#include <string>

// I/O - helpers

auto glob (const std::string& s_path) -> std::vector<std::uint8_t>;
void plop (const std::string& s_path, const std::vector<std::uint8_t>& s_out);
