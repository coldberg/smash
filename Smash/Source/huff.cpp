#include "huff.hpp"
#include <array>
#include <bitset>
#include <iomanip>

struct Huff_code {        
    std::uint64_t bits;
    std::uint8_t  size;
};

static void Huff_print_code (const Huff_code& s_code, std::uint8_t s_byte, std::uint64_t s_freq) {
    std::printf (s_byte >= 32 && s_byte < 128u ? " `%c`" : "0x%02X", s_byte);
    std::printf (" : %2d : %10llu : ", s_code.size, s_freq);
    for (auto i = 64u; i > 0; --i) {
        if (i == s_code.size) std::putc (' ', stdout);
        std::putc (s_code.bits & (1ull << (i - 1ull)) ? '1' : '0', stdout);
    }
    std::putc ('\n', stdout);
}

static void Huff_visit_tree_node (
    const std::uint16_t (&s_tree_topology) [256u][2u], 
    Huff_code (&s_code_table) [256u],
    std::uint16_t s_node,                                                     
    std::uint8_t  s_size,
    std::uint64_t s_code)
{
    if (s_node < 256u) {
        s_code_table [s_node].bits = s_code << (64ull - s_size);
        s_code_table [s_node].size = s_size;
        return;
    }

    Huff_visit_tree_node (
        s_tree_topology, 
        s_code_table, 
        s_tree_topology [s_node - 256u] [0u],
        s_size + 1ull,
        (s_code << 1ull)|0ull);

    Huff_visit_tree_node (
        s_tree_topology,
        s_code_table, 
        s_tree_topology [s_node - 256u] [1u],
        s_size + 1ull,
        (s_code << 1ull)|1ull);
}

static void Huff_encode_header (
    const std::uint16_t (&s_tree_topology) [256u] [2u], 
    std::uint32_t s_total_nodes,
    std::uint64_t s_total_dsize,
    std::vector<std::uint8_t>& s_encoded) 
{
    for (auto i = 0u; i < 8u; ++i) {
        s_encoded.push_back (s_total_dsize & 0xff);
        s_total_dsize >>= 8u;
    }

    s_encoded.push_back (s_total_nodes);
    for (auto i = 0u; i <= s_total_nodes; ++i) {
        s_encoded.push_back (s_tree_topology [i][0u] & 0xff);
        s_encoded.push_back (s_tree_topology [i][1u] & 0xff);
    }
    std::uint8_t s_bits = 0u;
    std::uint8_t s_bacc = 0u;
    for (auto i = 0u; i <= s_total_nodes; ++i) {
         auto s_b0 = ((s_tree_topology [i][0u] >> 8u) & 1u);
         auto s_b1 = ((s_tree_topology [i][1u] >> 8u) & 1u);
         s_bits = (s_bits << 2u) | (s_b1 << 1u) | s_b0;
         s_bacc += 2u;
         if (s_bacc >= 8u) {
             s_encoded.push_back (s_bits);
             s_bits = 0u;
             s_bacc = 0u;
         }
    }
    if (s_bacc > 0u) {
        s_encoded.push_back (s_bits);
    }
}

static void Huff_encode_pre_pass (
    const std::vector<std::uint8_t>& s_data,
    Huff_code (&s_code_table) [256u],
    std::vector<std::uint8_t>& s_encoded) 
{
    std::uint16_t s_tree_topology [256u] [2u] = {};
    std::uint64_t s_freq_counters [512u] = {};

    std::vector<std::uint16_t> s_fringe;
    s_fringe.reserve (256u);

    for (const auto& s_byte : s_data) {        
        ++s_freq_counters [s_byte];
        if (s_freq_counters [s_byte] == 1u) {
            s_fringe.push_back (s_byte);            
        }        
    }

    auto s_top_node = 255u;
    while (s_fringe.size () > 1u) {
        std::sort (s_fringe.begin (), s_fringe.end (), 
            [&s_freq_counters] (auto a, auto b) {
                return s_freq_counters [a] 
                     > s_freq_counters [b];
            });

        auto s_branch0 = s_fringe.back ();
        s_fringe.pop_back ();
        auto s_branch1 = s_fringe.back ();
        s_fringe.pop_back ();

        s_freq_counters [++s_top_node] 
            = s_freq_counters [s_branch0] 
            + s_freq_counters [s_branch1];

        s_tree_topology [s_top_node - 256u] [0] = s_branch0;
        s_tree_topology [s_top_node - 256u] [1] = s_branch1;

        s_fringe.push_back (s_top_node);
    }
    Huff_visit_tree_node (s_tree_topology, s_code_table, s_top_node, 0u, 0ull);
    Huff_encode_header (s_tree_topology, s_top_node - 256u, s_data.size (), s_encoded);

    //for (auto i = 0u; i < 256u; ++i) {
    //    if (s_freq_counters [i] < 1u)
    //        continue;
    //    Huff_print_code (s_code_table [i], i, s_freq_counters [i]);
    //}
}

static void Huff_encode_with_table (
    const std::vector<std::uint8_t>& s_data, 
    const Huff_code (&s_code_table) [256u],
    std::vector<std::uint8_t>& s_encoded) 
{    
    std::uint32_t s_bits_accumulated = 0u;
    std::uint8_t s_temp = 0u;
    for (const auto& s_byte : s_data) {
        auto s_size = s_code_table [s_byte].size;
        auto s_bits = s_code_table [s_byte].bits;
        for (auto i = 0; i < s_size; ++i) {
            s_temp = (s_temp << 1u) | ((s_bits & 0x8000000000000000) ? 1 : 0);
            s_bits <<= 1ull;
            ++s_bits_accumulated;
            if (s_bits_accumulated >= 8u) {
                s_encoded.push_back (s_temp);
                s_bits_accumulated = 0u;
                s_temp = 0u;
            }
        }
    }
    if (s_bits_accumulated) {
        s_encoded.push_back (s_temp);
    }
}

void Huff::encode (std::vector<std::uint8_t>& s_data) {
    std::vector<std::uint8_t> s_encoded;
    Huff_code s_code_table [256u];
    s_encoded.reserve (s_data.size ());
   
    Huff_encode_pre_pass (s_data, s_code_table, s_encoded);    
    Huff_encode_with_table (s_data, s_code_table, s_encoded);
    s_data = std::move (s_encoded);
}

void Huff::decode (std::vector<std::uint8_t>& s_data) {
    auto s_fsize = 0ull;
    for (auto i = 0u; i < 8u; ++i) {
        s_fsize |= (s_data [i] << (i * 8u));
    }


    __debugbreak ();
}

