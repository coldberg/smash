#pragma once
#include <vector>
#include <array>
#include <numeric>

// Move-To-Front Transform

struct Mtf {
    static void encode (std::vector<std::uint8_t>& s_data);
    static void decode (std::vector<std::uint8_t>& s_data);
};