#include "mtf.hpp"

void Mtf::encode (std::vector<std::uint8_t>& s_data) {
    std::array<std::uint8_t, 256u> s_lut;
    std::iota (s_lut.begin (), s_lut.end (), 0u);
    for (auto& s_byte : s_data) {
        auto s_it = std::find (s_lut.begin (), s_lut.end (), s_byte);
        std::copy (s_lut.begin (), s_it, s_lut.begin () + 1u);
        s_lut [0] = s_byte;
        s_byte = (std::uint8_t)std::distance (s_lut.begin (), s_it);
    }
}

void Mtf::decode (std::vector<std::uint8_t>& s_data) {
    std::array<std::uint8_t, 256u> s_lut;
    std::iota (s_lut.begin (), s_lut.end (), 0u);
    for (auto& s_byte : s_data) {
        auto s_tmp = s_byte;
        s_byte = s_lut [s_byte];
        std::copy (s_lut.begin (), s_lut.begin () + s_tmp, s_lut.begin () + 1u);
        s_lut [0] = s_byte;
    }
}
