#pragma once

#include <cstddef>
#include <vector>
#include <algorithm>

class Rle {
    static const auto st_thresshold = 3u;
    static const auto st_max_run_length = 128u;

    static std::size_t count_run_length (const std::vector<std::uint8_t>& s_data, std::size_t s_offset);

public:
    static void encode (std::vector<std::uint8_t>& s_data);
    static void decode (std::vector<std::uint8_t>& s_data);

    friend struct Tests;
    struct Tests {

        static void count_run_length (const std::vector<std::uint8_t>& s_test0);
        static void encode (std::vector<std::uint8_t>& s_data, const std::vector<std::uint8_t>& s_targ);
        static void decode (std::vector<std::uint8_t>& s_data, const std::vector<std::uint8_t>& s_orig);
        static void run ();

    };
};

