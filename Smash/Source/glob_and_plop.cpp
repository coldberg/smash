#include "glob_and_plop.hpp"
#include <fstream>

auto glob (const std::string & s_path) -> std::vector<std::uint8_t> {
    std::ifstream s_instream (s_path, std::ios::binary|std::ios::ate);
    std::vector<std::uint8_t> s_data;
    if (s_instream.good ()) {
        std::size_t s_length = s_instream.tellg ();
        s_instream.seekg (0u, std::ios::beg);
        s_data.resize (s_length);
        s_instream.read (reinterpret_cast<char*> (s_data.data ()), s_length);
    }
    return s_data;
}

void plop (const std::string & s_path, const std::vector<std::uint8_t>& s_out) {
    std::ofstream s_outstream (s_path, std::ios::binary);
    s_outstream.write (reinterpret_cast<const char*>(s_out.data ()), s_out.size ());
}
