#pragma once

#include <chrono>
#include <iostream>

template <typename _Function>
inline void time_it (_Function&& s_fun, const std::string& s_label) {
    using namespace std::chrono;
    auto t0 = high_resolution_clock::now ();
    s_fun ();
    auto t1 = high_resolution_clock::now ();
    std::cout << s_label << " : " << duration_cast<milliseconds> (t1 - t0).count () << " ms\n";
}

#define TIME_IT(...) time_it([&] () { __VA_ARGS__; }, #__VA_ARGS__)

