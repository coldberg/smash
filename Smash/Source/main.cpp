#include <numeric>
#include <array>
#include <algorithm>
#include <cassert>
#include <vector>
#include <string>
#include <cassert>

#include "time_it.hpp"
#include "glob_and_plop.hpp"
#include "mtf.hpp"
#include "rle.hpp"
#include "huff.hpp"

int main (int, char**) {
    std::string n = "bible.txt";
    auto s = glob (n);
   
   
    Huff::encode (s);
    plop (n + ".huff", s);
    Huff::decode (s);
    plop (n + ".orig", s);

    //Mtf::encode (s);
    //Rle::encode (s);

    

    ////Rle::Tests::run ();
    //
    //TIME_IT (Mtf::encode (s));
    //TIME_IT (Rle::encode (s));    
    //plop (n + ".rle", s);
    //
    //TIME_IT (Rle::decode (s));    
    //TIME_IT (Mtf::decode (s));
    //plop (n + ".org", s);


    return 0;
}
